#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void mergeSort(int *array, int inicio, int fim);
void mergeInPlace(int *array, int inicio, int meio, int fim);
void insertSort(int *array, int inicio, int fim);
void randomizeArray(int *array, int tamanho);
void printArray(int *array, int tamanho);

int main(int argc, char *argv[])
{
    int tamanho = 10;
    int *array = malloc(tamanho * sizeof(int));

    randomizeArray(array, tamanho);
    printf("Array aleatório: ");
    printArray(array, tamanho);

    mergeSort(array, 0, tamanho - 1);
    printf("Array ordenado: ");
    printArray(array, tamanho);

    free(array);
    return 0;
}

void mergeSort(int *array, int inicio, int fim)
{
    int meio;

    if (inicio < fim)
    {
        meio = (inicio + fim) / 2;
        mergeSort(array, inicio, meio);
        mergeSort(array, meio + 1, fim);
        if (array[meio] > array[meio + 1])
        {                                           // verifica se já está ordenado
            mergeInPlace(array, inicio, meio, fim); // realiza a operação merge sem auxiliar
        }
    }
    else
    {
        insertSort(array, inicio, fim); // utiliza insertsort em partes pequenas do array
    }
}

void mergeInPlace(int *array, int inicio, int meio, int fim)
{
    int i = inicio;
    int j = meio + 1;

    while (i <= meio && j <= fim)
    {
        if (array[i] <= array[j])
        {
            i++;
        }
        else
        {
            int temp = array[j];
            for (int k = j; k > i; k--)
            {
                array[k] = array[k - 1];
            }
            array[i] = temp;
            i++;
            meio++;
            j++;
        }
    }
}

void insertSort(int *array, int inicio, int fim)
{
    int i, chave, j;
    for (i = inicio + 1; i <= fim; i++)
    {
        chave = array[i];
        j = i - 1;

        while (j >= inicio && array[j] > chave)
        {
            array[j + 1] = array[j];
            j = j - 1;
        }
        array[j + 1] = chave;
    }
}

void randomizeArray(int *array, int tamanho)
{
    srand(time(NULL));
    for (int i = 0; i < tamanho; i++)
    {
        array[i] = rand() % 100;
    }
}

void printArray(int *array, int tam)
{
    for (int i = 0; i < tam; i++)
    {
        printf("%d ", array[i]);
    }
}
